#include "common.h"     // kvuli definici movych typu
#include "disk.h"

#define TBL_SIZE 513

char znak[TBL_SIZE];    // huffmanuv strom
uint otec[TBL_SIZE];
uint syn [TBL_SIZE];
long vaha[TBL_SIZE];
uint NOVY;              // uzel specialniho znaku

/* =============== spolecne pro kompresi i dekompresi ============= */

void aktualizuj(int v)
{
  uchar z;
  uint s;
  uint i;

  while (v>0)                           // v neni koren
  {
    for (i=v; vaha[i-1]==vaha[v]; i--) ;// najdi v s nejnizsim poradim a stejnou vahou
    if (i!=v && i!=otec[v])
    {
      z=znak[i];                        // vymen v a i (cele podstromy)
      znak[i]=znak[v];
      znak[v]=z;
      s=syn[i];
      syn[i]=syn[v];
      syn[v]=s;
      if (syn[v] != TBL_SIZE)
        otec[syn[v]-1]=otec[syn[v]]=v;
      if (syn[i] != TBL_SIZE)
        otec[syn[i]-1]=otec[syn[i]]=i;
      v=i;
    }
    vaha[v]++;
    v=otec[v];
  }
  vaha[v]++;
}
void pridej_vrchol(char zn)             // pridava k uzlu NOVY
{
  syn[NOVY]=NOVY+2;
  syn[syn[NOVY]-1]=syn[syn[NOVY]]=TBL_SIZE;
  otec[syn[NOVY]-1]=otec[syn[NOVY]]=NOVY;
  vaha[syn[NOVY]-1]=1;
  vaha[syn[NOVY]]=0;
  znak[syn[NOVY]-1]=zn;
  NOVY=syn[NOVY];
}

/* ========================== komprese ============================ */

int najdi_vrchol(char zn)
{
  uint v;
  for (v=0; v<NOVY && znak[v]!=zn; v++) ;
  return v;
}

void zapis_kod(int v)
{
  if (v>0)                              // v neni koren
  {
    zapis_kod(otec[v]);
    if (v == syn[otec[v]]) zapis_nbit(1,0);
    else zapis_nbit(1,1);               // otec[v] = syn[otec[v]-1]
  }
}

void reset_huffman(void)
{
  int i;

  for (i=0; i < TBL_SIZE; i++)
  {
    znak[i]=1;
    vaha[i]=0;
    otec[i]=0;
    syn[i]=0;
  }
  NOVY=0;
}

void huf_encode(ulong *f_size)
{
  uchar p;
  int v;

  reset_file();
  reset_huffman();
  while (cti_nbit(8,&p))
  {
    if ((v=najdi_vrchol(p)) == NOVY)
    {
      zapis_kod(NOVY);
      zapis_nbit(8,p);
      pridej_vrchol(p);
    }
    else zapis_kod(v);
    aktualizuj(v);
  }
  flush();
  *f_size=wr_size;
}

/* ========================= dekomprese =========================== */

void huf_decode(ulong f_size)
{
  uchar p;
  int v=0;

  reset_file();
  reset_huffman();
  cti_nbit(8,&p);
  zapis_nbit(8,p);
  pridej_vrchol(p);
  aktualizuj(0);
  while (wr_size < f_size)            // dokud nezapsal tolik co zakodoval
  {
    while (syn[v] != TBL_SIZE)  // precti kod (dokud neni list)
    {
      cti_nbit(1,&p);
      if (p == 0) v=syn[v];
      else        v=syn[v]+1;
    }
    if (v==NOVY)
    {
      cti_nbit(8,&p);
      pridej_vrchol(p);
    }
    else
      p=znak[v];
    zapis_nbit(8,p);
    aktualizuj(v);
    v=0;
  }
  flush();
}
